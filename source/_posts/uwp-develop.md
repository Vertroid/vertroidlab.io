title: UWP开发笔记

categories:

- Desktop

- UWP

tags:

- C++/CX
- UWP
- Windows

---

本文主要记录UWP开发。

<!-- more -->



[TOC]

---

## 简介

一次开发，多处部署，支持任何Windows 10设备。

多种开发形式

- XAML + C#
- XAML + C++/CX
- HTML + JS

### 开发者模式

确认在设置中开启了开发者模式。



### 官方文档

https://dev.windows.com



## 工程

### App.xaml

### MainPage.xaml



## XAML

XML子集，与定义用户界面有关。

### 纲要（Schema）

通过纲要来约定各类命名空间。通常不去修改即可。解析器解析时会从纲要中寻找定义。

- 命名空间：在纲要中定义，例如`x:Name`中的`x`就是一种命名空间。

### 类型转换器

将XAML中的字符串属性转换为枚举、对象等等，保持代码整洁。解析器会替我们做好事情。



### 默认属性

大部分控件都有默认属性，例如`Content`。

### Code behind

每个XAML页面都会有同名代码，例如`MainPage.xaml`通常会有同名的代码文件`MainPage.xaml.cs`或`MainPage.xaml.cpp`等等。



## 程序结构

- Application
- Window
- Frame
- Page：每个`Page`只能定义一个`Content`，即一个子节点。

## 布局

布局控件没有`Content`属性。

### Grid



网格状控件，可分成多行多列。内部控件默认会重叠。

- `Grid.RowDefinition`
- `Grid.ColumnDefinition`

`Grid`有几种方法定义单元格大小。

- 星号（`*`）：均分空间大小。
- 自动（Auto）：根据内部控件大小调整
- 具体大小：固定单元格大小。

### StackPanel

栈型控件，内部空间会排列累加，无法重叠。

部分常用属性：

- `Orientation`

### RelativePanel

该控件内的其他控件可以通过设置`RelativePanel.AlignXXX`来选择对齐方式。



### SplitView

导航界面控件，包含两个子项：

- `SplitView.Pane`
- `SplitView.Content`

常用属性：

- `DisplayMode`
  - `Inline`：`Pane`隐藏，显示时推开`Content`。
  - `Overlay`：`Pane`隐藏，显示时覆盖。
  - `CompactInline`：根据`CompactPaneLength`设置会显示部分`Pane`，其他与`Inline`一致。
  - `CompactOverlay`：根据`CompactPaneLength`设置会显示部分`Pane`，其他与`Overlay`一致。

#### ListView

- `IsItemClickEnabled`：设为`true`时才能触发`ListViewItem`的点击事件。



## 样式

### ResourceDictionary



## 导航



## 输入

### CheckBox

快速点击会有问题？





