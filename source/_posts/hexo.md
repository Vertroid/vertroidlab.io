title: Hexo博客配置

categories:

- Hexo

tags: 

- 博客
- Hexo

---

本文主要说明Hexo部署时要求。



<!-- more -->

[toc]



## 安装

### 准备

安装Git，Nodejs。

安装Hexo

```bash
npm install -g hexo-cli 
```

### 初始化

```bash
hexo init myblog # myblog替换成任意自己的博客名称
cd myblog
npm install
```

初始化后目录结构

- `public`：生成的页面

- `source`：存放文章

- `themes`：主题

- `scaffolds`：文章模板

## 基础

### _config.yml

Hexo配置文件

- `post_meta`：文章默认信息，例如创建时间、修改时间等等

### 生成

```bash
hexo generate # hexo g
```

### 清理

```bash
hexo clean # 清空public
```

### 本地服务

```bash
hexo server # hexo s
```

### 部署

`_config.yml`中设置`deploy`部分

```yml
deploy:
	type: # Git
	repo: # Git repository
	# branch: master
```

在Bash输入指令，可能需要输入用户名密码

```
hexo deploy # hexo d
```

## 内容

### Front-matter

打开任意文章，文章开头处进行修改

```bash
title: '' # 标题
date: 2020/1/1 20:00:00 # 时间
categories: # 目录
- cat1
- cat2
tags: # 标签
- tag1
- tag2
---
```

### 文章

```bash
hexo new page 'name' # name指定页面名称
hexo new draft 'name'
```

### 目录

使用目录前必须创建

```bash
hexo new page categories 
```

修改`./source/categories/index.md`

```bash
title: '' # 目录页标题
type: categories
layout: categories
```

### 标签

使用标签前必须创建

```bash
hexo new page tags # 
```

修改`./source/categories/index.md`

```bash
title: '' # 标签页标题
type: tags
layout: tags
```



## 部署

### Gitlab Pages

在Gitlab创建`username.gitlab.io`库，并将Hexo工程通过Git上传到该库中

打开Hexo工程内`_config.yml`，设置`url`为`https://username.gitlab.io`，设置`root`为`/`

Hexo工程内创建`_gitlab-ci.yml`

```yaml
# 选择镜像
image: node:10-alpine 
cache:
    paths:
        - node_modules/

# 执行前指令
before_script: 
    - npm install hexo-cli -g
    - test -e package.json && npm install
    - hexo generate
    
# 部署
pages:
    script:
        - hexo deploy
    artifacts:
        paths:
            - public
    only:
        - master
```

上传后等待管道执行，确保通过，然后访问`username.gitlab.io`

## 功能

### 搜索

```bash
npm install hexo-generator-searchdb --save
```

修改`_config.yml`：

```bash
# 搜索
# 如果没有该字段，则添加
search:
	path: search.xml
	field: post
	format: html
	limit: 10000
```

修改相关主题`_config.yml`，使其支持本地搜索。

### 静态资源压缩

安装Gulp

```bash
# Gulp本体
npm install -g gulp
# Gulp插件
npm install --save gulp-minify-css
npm install --save gulp-uglify
npm install --save gulp-htmlmin
npm install --save gulp-htmlclean
npm install --save gulp-imagemin
```

添加`gulpfile.js`文件

```javascript
var gulp = require('gulp');
var minifycss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var htmlmin = require('gulp-htmlmin');
var htmlclean = require('gulp-htmlclean');
var imagemin = require('gulp-imagemin');

// 压缩CSS
gulp.task('minify-css', function(){
    return gulp.src('./public/**/*.css')
    .pipe(minifycss())
    .pipe(gulp.dest('./public'));
});

// 压缩HTML
gulp.task('minify-html', function(){
    return gulp.src('./public/**/*.html')
    .pipe(htmlclean())
    .pipe(htmlmin({
        removeComments: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true
    }))
    .pipe(gulp.dest('./public'));
});

// 压缩JS
gulp.task('minify-js', function(){
    return gulp.src(['./public/**/*.js', '!./public/**/*min.js'])
    .pipe(uglify())
    .pipe(gulp.dest('./public'));
});

// 压缩图片
gulp.task('minify-images', function(){
    return gulp.src('./public/demo/**/*.*')
    .pipe(imagemin({
        optimizationLevel: 5,
        progressive: true,
        interlaced: false,
        multipass: false
    }))
    .pipe(gulp.dest('./public/uploads'))
});

```

部署前执行Gulp

```bash
hexo clean
hexo g
gulp
```



## 主题

### NexT

#### 配置文件

- `_config.yml`
  - `scheme`：主题风格
  - `menu`：菜单项
  - `back2top`：回到顶部
  - `pace`：顶部加载条
  - `local_search`：本地搜索

