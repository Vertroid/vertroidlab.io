var gulp = require('gulp');
var minifycss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var htmlmin = require('gulp-htmlmin');
var htmlclean = require('gulp-htmlclean');
var imagemin = require('gulp-imagemin');

// 压缩CSS
gulp.task('minify-css', function(){
    return gulp.src('./public/**/*.css')
    .pipe(minifycss())
    .pipe(gulp.dest('./public'));
});

// 压缩HTML
gulp.task('minify-html', function(){
    return gulp.src('./public/**/*.html')
    .pipe(htmlclean())
    .pipe(htmlmin({
        removeComments: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true
    }))
    .pipe(gulp.dest('./public'));
});

// 压缩JS
gulp.task('minify-js', function(){
    return gulp.src(['./public/**/*.js', '!./public/**/*min.js'])
    .pipe(uglify())
    .pipe(gulp.dest('./public'));
});

// 压缩图片
gulp.task('minify-images', function(){
    return gulp.src('./public/demo/**/*.*')
    .pipe(imagemin({
        optimizationLevel: 5,
        progressive: true,
        interlaced: false,
        multipass: false
    }))
    .pipe(gulp.dest('./public/uploads'))
});
